# iOS CFW

### attempt to downgrade iPhone6,2 iOS 12.4.2 to 10.3.3 without or modified Setup.app
### i do this on an iPhone that is owned by me and iCloud Locked by me
### only for research purpose


1. create iBSS and iBEC from ipsw with keys for iPhone6,2

```bash
./img4 -i backup/iBSS.iphone6.RELEASE.im4p -o decrypt_iBSS.iphone6.RELEASE.im4p -k "f2aa35f6e27c409fd57e9b711f416cfe599d9b18bc51d93f2385fa4e83539a2eec955fce5f4ae960b252583fcbebfe75" -D
./img4 -i backup/iBEC.iphone6.RELEASE.im4p -o decrypt_iBEC.iphone6.RELEASE.im4p -k "75a06e85e2d9835827334738bb84ce7315c61c585d30ab07497f68aee0a64c433e4b1183abde4cfd91c185b9a70ab91a" -D

./img4tool -e -o raw_decrypt_iBSS.iphone6.RELEASE.im4p decrypt_iBSS.iphone6.RELEASE.im4p
./img4tool -e -o raw_decrypt_iBEC.iphone6.RELEASE.im4p decrypt_iBEC.iphone6.RELEASE.im4p

./iBoot64patcher raw_decrypt_iBSS.iphone6.RELEASE.im4p pwn_raw_decrypt_iBSS.iphone6.RELEASE.im4p
./iBoot64patcher raw_decrypt_iBEC.iphone6.RELEASE.im4p pwn_raw_decrypt_iBEC.iphone6.RELEASE.im4p

./img4tool -p hax_iBSS.iphone6.RELEASE.im4p --tag ibss --info iBoot-hax pwn_raw_decrypt_iBSS.iphone6.RELEASE.im4p
./img4tool -p hax_iBEC.iphone6.RELEASE.im4p --tag ibec --info iBoot-hax pwn_raw_decrypt_iBEC.iphone6.RELEASE.im4p
```


2. pwn DFU and get unsigned exec

```bash
./ipwndfu -p
python rmsigchks.py
```

3. get current nonce and request a blob with it

```bash
./igetnonce
./tsschecker -e "1b6f4401ba4" -s -o -i 9.9.10.3.3 --buildid 14G60 -d iPhone6,2 --save-path "." --apnonce "" --sepnonce "" --boardconfig n53ap
```

4. use said blob to sign iBSS and iBEC for use inside ipsw and for sending with irecovery

```bash
img4tool -s blob.shsh -c signed_hax_iBSS.img4 -p hax_iBSS.iphone6.RELEASE.im4p
img4tool -s blob.shsh -c signed_hax_iBEC.img4 -p hax_iBEC.iphone6.RELEASE.im4p
```

5. upload signed iBSS and iBEC to get into pwnRecovery mode

```bash
irecovery -f text.txt
irecovery -f signed_hax_iBSS.iphone6.RELEASE.im4p
irecovery -f signed_hax_iBEC.iphone6.RELEASE.im4p
```

6. create CFW with custom signed iBSS and iBEC

rename ".ipsw" to ".zip" and edit with 7zip
replace /Firmware/dfu/iBSS... with custom signed ones
replace /Firmware/dfu/iBEC... with custom signed ones

```bash
hdiutil attach -owners on /path/to/your.dmg -shadow
```
edit the new .dmg
```bash
hdiutil detach /dev/disk<number>
hdiutil convert -format UDZO -o /path/to/new.dmg /path/to/your.dmg -shadow
```

rename ".zip" to ".ipsw"

7. restore with futurerestore

```bash
./futurerestore -t blob.shsh -b baseband.bbfw -p OTA.plist -s sep.im4p -m OTA.plist patched.ipsw
```

## current ToDO List

1. curently cant get iBEC to boot (phone lights up with empty screens)
2. need to push ibss and ibec and create ipsw with signed ibss and ibec
3. restore
4. 

### extra info

irecovery -q | grep NONC
irecovery -c 'sentenv com.apple.System.boot-nonce 0x286923e35694dc4a' -v 
irecovery -c 'saveenv' -v 
irecovery -c 'setenv auto-boot false' -v 
irecovery -c 'saveenv' -v 
irecovery -c 'reset' -v 
irecovery -q | grep NONC
